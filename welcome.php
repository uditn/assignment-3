<!-- Including header file -->
<?php
    session_start();
    if(empty($_SESSION['username'])){
        header("Location: login.php?err=You need to be login first");
    }
    $set_header = "welcome.php";
    $title = "YouthClub";
    include 'header.php';
?>

<!-- Body part -->
<body id="body_welcome">
        <h1 style = "color: black; align:center; margin: 12%; margin-left:41%;"><b>Welcome <?php echo $_SESSION['username']; ?></b></h1>
</body>

<!-- Including footer part -->
<?php
    include 'footer.php';
?>
