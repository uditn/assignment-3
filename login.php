<!-- Including header file -->
<?php
    $title = "Login Here";
    include 'header.php';
?>

<!-- Body part -->
<body id="body_login">
        <div class="container form-container">
            <div id="login-row" class="row justify-content-center align-items-center">
                <div id="login-column" class="col-md-9">
                    <div id="login-box" class="col-md-12">
                        <form id="login-form" class="form" action="validate.php" method="post">
                            <div style="color : red; text-align: center; background-color:orange"><b><?php if(isset($_REQUEST['err'])){echo $_REQUEST['err'];} ?><b></div>
                            <h3 class="text-center text-info">Login</h3>
                            <div class="form-group">
                                <label for="username" class="text-info">Username:</label>
                                <input type="text" name="username" id="username" class="form-control">
                                <div style="color : red"> <?php if(isset($_REQUEST['username_err'])){echo $_REQUEST['username_err'];} ?> </div>
                            </div>
                            <div class="form-group">
                                <label for="password" class="text-info">Password:</label>   
                                <input type="text" name="password" id="password" class="form-control">
                                <div style="color : red"> <?php if(isset($_REQUEST['password_err'])){echo $_REQUEST['password_err'];} ?> </div>
                            </div>
                            <div class="form-group" style="text-align:center">
                                    <input type="submit" name="submit" class="btn btn-info btn-md" value="submit">
                            </div>
                            <div id="register-link" class="text-right">
                                <a href="#" class="text-info">Register here</a>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
</body>

<!-- Including footer part -->
<?php
    include 'footer.php';
?>
