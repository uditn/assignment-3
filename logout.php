<?php 
    session_start();
    if(!empty($_SESSION['username']))
    {
        session_unset();
        session_destroy();
        header("Location: index.php");
    }else
        header("Location: index.php");  
?>